<?php

namespace App;

use RuntimeException;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Yaml\Yaml;

final class WatcherCommand extends Command
{
    public static $defaultName = 'watcher';

    private array $disableList = [];

    protected function configure(): void
    {
        $this
            ->addOption('directory', 'd', InputOption::VALUE_REQUIRED, 'Output directory', '/')
            ->addOption('pid', 'p', InputOption::VALUE_REQUIRED, 'Bind9 pid file', '/var/run/named/named.pid')
            ->addArgument('config', InputArgument::REQUIRED, 'Config yaml')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $configFile   = $input->getArgument('config');
        $bind9FilePid = $input->getOption('pid');
        $config       = Yaml::parseFile($configFile);
        $disableList  = [];

        while (true) {
            foreach ($config['dns'] as $domain => $domainConf) {
                foreach ($domainConf['default'] as $ipAddress) {
                    if (Process::fromShellCommandline(sprintf('ping -c 3 %s', $ipAddress))->run() !== 0) {
                        $disableList[$ipAddress] = $domain;
                    }
                }
            }

            if ($this->disableList !== $disableList) {
                $arguments = [
                    '-e' => array_keys($this->disableList),
                    '-d' => $input->getOption('directory'),
                    'config' => $input->getArgument('config'),
                ];
                $application = $this->getApplication();
                if (!$application instanceof Application) {
                    throw new RuntimeException('Application is required.');
                }
                $application->find(GenerateConfigCommand::$defaultName)->run(new ArrayInput($arguments), $output);
                Process::fromShellCommandline(sprintf('kill SIGHUB $(cat %s)', $bind9FilePid))->run();
                $this->disableList = $disableList;
                $disableList = [];
            }

            sleep(20);
        }
    }
}