<?php

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

final class GenerateConfigCommand extends Command
{
    public static $defaultName = 'generate';

    private array $files = [];

    protected function configure(): void
    {
        $this
            ->addOption('directory', 'd', InputOption::VALUE_REQUIRED, 'Output directory', '/')
            ->addOption('exclude', 'e', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED, 'Exclude ip address')
            ->addArgument('config', InputArgument::REQUIRED, 'Config yaml')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $configFile      = $input->getArgument('config');
        $outputDirectory = realpath($input->getOption('directory'));
        $excludeAddress  = $input->getOption('exclude');

        $config = Yaml::parseFile($configFile);

        $this->files['/etc/bind/config/acl.conf'] = $this->generateAcls($config['acl']);
        $this->files['/etc/bind/config/names.conf'] = $this->generateNamedConfig($config['dns'], $outputDirectory, $excludeAddress);

        foreach ($this->files as $path => $content) {
            $dir =\dirname($outputDirectory . $path);
            $path = $outputDirectory . $path;
            if (@!mkdir($dir, 0777, true) && !is_dir($dir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
            }
            file_put_contents($path, $content);
        }

        return 0;
    }

    private function generateAcl(string $name, array $conditions): string
    {
        $geoIpConditions = [];
        foreach ($conditions as $nameCondition => $values) {
            foreach ($values as $value) {
                $value = str_contains($value, ' ') ? sprintf('"%s"', $value) : $value;
                $geoIpConditions[] = sprintf('    geoip %s %s;', $nameCondition, $value);
            }
        }

        $geoIpConditions = implode(PHP_EOL, $geoIpConditions);

        $config = <<<CONF
acl "$name" {
$geoIpConditions
};

CONF;

        return $config;
    }

    private function generateAcls(array $acls): string
    {
        $acl = [];
        foreach ($acls as $name => $conditions) {
            $acl[] = $this->generateAcl($name, $conditions);
        }

        return implode(PHP_EOL, $acl);
    }

    private function generateFileDB(string $domain, array $names): string
    {
        $list = [];
        foreach ($names as $name => $value) {
            $name   = $name === 'any' ? '@' : $name;
            $list[] = sprintf('%s IN    A %s', $name, $value);
        }

        $namesRecords = implode(PHP_EOL, $list);

        $config = <<<CONF
\$TTL    60
@   IN  SOA $domain.    admin.$domain. (
          2     ; Serial
     604800     ; Refresh
      86400     ; Retry
    2419200     ; Expire
     604800 )   ; Negative Cache TTL

; name servers - NS records
@   IN  NS  $domain.

; name servers - A records
$namesRecords

CONF;

        return $config;
    }

    private function generateLocaleDB(string $domain, array $configs, array $excludeAddress = []): array
    {
        $default = $configs['default'];

        foreach ($configs as $name => $config) {
            if ($name === 'default' || in_array($config['any'], $excludeAddress, true)) {
                continue;
            }

            $config        = array_merge($default, $config);
            $result[$name] = $this->generateFileDB($domain, $config);
        }

        $result['default'] = $this->generateFileDB($domain, $default);

        return $result;
    }

    private function generateNamedConfig(array $dns, string $outputDirectory, array $excludeAddress = []): string
    {
        $acls = [];
        foreach ($dns as $domainName => $domainConfig) {
            $domainDb = $this->generateLocaleDB($domainName, $domainConfig, $excludeAddress);
            foreach ($domainDb as $aclName => $dbRow) {
                $acls[$aclName][$domainName] = $dbRow;
            }
        }

        $config = [];
        foreach ($acls as $aclName => $domains) {
            $matchClients = $aclName === 'default' ? 'any' : $aclName;

            $zone = [];
            foreach ($domains as $domainName => $conf) {
                $file = '/etc/bind/config/locales/' . $domainName . '/' . $aclName . '.db';
                $this->files[$file] = $conf;
                $zone[] = sprintf('    zone "%s" { type master; file "%s"; };', $domainName, $outputDirectory . $file);
            }

            $zoneRow  = implode(PHP_EOL, $zone);
            $config[] = <<<CONF
view "$aclName" {
    match-clients { $matchClients; };
$zoneRow
};

CONF;
        }

        return implode(PHP_EOL, $config);
    }
}