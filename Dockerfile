FROM php:7.4-cli as build

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/install-php-extensions
COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

RUN apt update \
    && apt install -y git \
    && install-php-extensions yaml zip

RUN composer global require --dev bamarni/composer-bin-plugin \
    && composer global bin php-scoper config minimum-stability dev \
    && composer global bin php-scoper config prefer-stable true \
    && composer global bin php-scoper require --dev humbug/box

COPY ./ /usr/lib/named-dns-configurator
WORKDIR /usr/lib/named-dns-configurator

RUN composer install \
    && composer global exec box compile \
    && mv named-dns-configurator.phar /usr/bin/named-dns-configurator

FROM php:7.4-cli-alpine

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/install-php-extensions

COPY --from=build /usr/bin/named-dns-configurator /usr/bin/named-dns-configurator

ENTRYPOINT ["/usr/bin/named-dns-configurator"]
