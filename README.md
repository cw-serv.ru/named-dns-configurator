# Named Geo-DNS Configurator
Программа служит для быстрой конвертации кофигов из `yaml` формата

## Пример
`config.yaml`
```yaml
acl:
  france:
    country: [ "FR", "DE", "IT", "SE", "PT", "GR", "DK", "NL", "ES", "IE", "BE", "PL", "LV", "CZ", "HU", "SK", "BG", "RO", "AT", "SI", "FI", "EE", "LT", "LU", "MT", "HR" ]
  moscow:
    city: [ "Moscow" ]
  ukraine:
    country: [ "UA" ]
  novosibirsk:
    city: [ "Pechora", "Usinsk", "Maksim", "Asha", "Sova", "Krasnovishersk", "Novostroy", "Posad", "Berëzovaya", "Lysva", "Sarana", "Yuva", "Beloretsk", "Kalinovka", "Gremyachinsk", "Laki", "Kizel", "Gornozavodsk", "Chusovoy", "Gubakha", "Soya", "Krasnoufimsk", "Gromovoy", "Novotroitsk", "Yuryuzan", "Vyazovaya", "Sara", "Aleksandrovsk", "Gai", "Abzakovo", "Orsk", "Magnitogorsk", "Prostory", "Kachkanar", "Satka", "Vels", "Uchaly", "Sibay", "Nizhniye Sergi", "Forshtadt", "Kemer", "Arkhangelsk", "Uralskiy", "Miass", "Krasnoyarskiy", "Nizhniy Tagil", "Novouralsk", "Platina", "Krasnoturinsk", "Revda", "Chernoistochinsk", "Kushva", "Zlatoust", "Polevskoy", "Krasnouralsk", "Nevyansk", "Nikolo-pavlovskoye", "Karabash", "Severka", "Kirovgrad", "Pervouralskiy", "Inta", "Chebarkul", "Turgoyak", "Varnek", "Baranchinskiy", "Yasnyy", "Verkhniy Ufaley", "Arkhangelsk", "Cheremshanka", "Is", "Chelyabinsk", "Yekaterinburg", "Serov", "Kyshtym", "Salda", "Malysheva", "Asbest", "Snezhinsk", "Rezh", "Monetny", "Kedr", "Karmanov", "Kedrovoye", "Yemanzhelinka", "Zauralskiy", "Yuzhnouralsk", "Korkino", "Berëzovskiy", "Bolshoy Istok", "Koltsovo", "Ozërsk", "Uvelskiy", "Roshchino", "Troitsk", "Kopeysk", "Bogdanovich", "Kamensk-uralskiy", "Kuri", "Molva", "Artemovskiy", "Sukhoy Log", "Pospelova", "Alapayevsk", "Yugorsk", "Nagibina", "Dalmatovo", "Shumikha", "Vorkuta", "Tomskiy", "Sovetskiy", "Kommunisticheskiy", "Shadrinsk", "Butka", "Nikita", "Zelenoborsk", "Borovikova", "Kurgan", "Krasnyy Oktyabr", "Uray", "Kirsanova", "Tyumen", "Berkut", "Kinder", "Labytnangi", "Borovskiy", "Pospelova", "Kharp", "Moskva", "Salekhard", "Sadovod", "Tobolsk", "Rynki", "Kirovskaya", "Khanty-mansiysk", "Turtas", "Cheremshanka", "Chelyuskintsev", "Salym", "Omsk", "Surgut", "Nadym", "Nefteyugansk", "Drovyanoy", "Azovo", "Pyt-yakh", "Yugan", "Mokhovaya", "Pangody", "Atak", "Megion", "Noyabrsk", "Stanovo", "Nizhnevartovsk", "Purpe", "Novyy Urengoy", "Urengoy", "Tarko-sale", "Strezhevoy", "Mamon", "Rabochiy", "Severka", "Musy", "Petrogradskiy", "Rubtsovsk", "Romanovo", "Respublikanskiy", "Kenga", "Kamen-na-obi", "Kochenëvo", "Chik", "Novosibirsk", "Berdsk", "Amba", "Skala", "Krasnoobsk", "Koltsovo", "Iskitim", "Sokur", "Saray", "Linëvo", "Sokolovo", "Romanovo", "Barnaul", "Nagornyy", "Krasnoyarskiy", "Tomsk", "Biysk", "Seversk", "Zarinsk", "Bogashevo", "Yurga", "Samus", "Sokolovo", "Belokurikha", "Kaftanchikovo", "Kemerovo", "Gorno-altaysk", "Nikitinskiy", "Belovo", "Taiga", "Dudinka", "Anzhero-sudzhensk", "Leninsk-kuznetskiy", "Topki", "Beket", "Yasnaya Polyana", "Berezovaya", "Kurganovka", "Aya", "Ust-koksa", "Novokuznetsk", "Prokopyevsk", "Kiselëvsk", "Kaltan", "Malinovka", "Noril'sk", "Mezhdurechensk", "Sheregesh", "Kayerkan", "Mariinsk", "Talnakh", "Tashtagol", "Orton", "Chernenko", "Achinsk", "Yaga", "Abakan", "Sayanogorsk", "Chernogorsk", "Surikova", "Ust-abakan", "Minusinsk", "Divnogorsk", "Lesosibirsk", "Berezovaya", "Eniseisk", "Sosnovoborsk", "Elita", "Tatarskaya", "Berëzovaya", "Mana", "Kuragino", "Ovsyanka", "Krasnoyarsk", "Kyzyl", "Zheleznogorsk", "Zelenogorsk", "Kansk", "Ilanskiy", "Boguchany", "Taishet", "Kodinsk", "Nizhneudinsk", "Vikhorevka", "Ovchinnikovskiy", "Bratsk", "Zima", "Irkutsk", "Angarsk", "Kuda", "Bard", "Tarasa", "Sludyanka", "Shelekhov", "Igirma", "Usolye-sibirskoye", "Zheleznogorsk-ilimskiy", "Baykal", "Armak", "Ust-kut", "Boyarskiy", "Kolobki", "Krasny Yar", "Selenginsk", "Gusinoozersk", "Kabansk", "Ulan-ude", "Turuntaevo", "Severobaykalsk", "Suvorova", "Barguzin", "Arta", "Aykhal", "Goryachiy", "Chita", "Bodaibo", "Mirnyy", "Taksimo", "Lensk", "Kuanda", "Shilka", "Zabaykalsk", "Chernyshevsk", "Chara", "Novaya Chara", "Krasnokamensk", "Akana", "Mogocha", "Grishko", "Skovorodino", "Tynda", "Aldan", "Neryungri", "Adam", "Belogorsk", "Blagoveshchensk", "Shimanovsk", "Svobodnyy", "Moro", "Kavkazskiy", "Yakutsk", "Progress", "Mayya", "Markha", "Slavyanka", "Vityaz", "Romashka", "Amurzet", "Posyet", "Kraskino", "Barabash", "Slavyansk", "Vladivostok", "Artëm", "Bolshoy Kamen", "Ussuriysk", "Pokrovka", "Yaroslavskiy", "Korsakovka", "Galenki", "Tavrichanka", "Trudovoye", "Volno-nadezhdinskoye", "Shkotovo", "Knevichi", "Khorol", "Novoshakhtinskiy", "Sibirtsevo", "Kamen-rybolov", "Tikhookeanskiy", "Birobijan", "Nakhodka", "Yakovlevka", "Vrangel", "Chernigovka", "Urgal", "Lesozavodsk", "Chegdomyn", "Fridman", "Zolotaya Dolina", "Leninskoe", "Arsenyev", "Vladimiro-aleksandrovskoye", "Spassk-dalniy", "Novyy Urgal", "Kirovskiy", "Luchegorsk", "Dalnerechensk", "Khabarovsk", "Olga", "Entuziastov", "Kavalerovo", "Roshchino", "Dalnegorsk", "Plastun", "Komsomolsk-na-amure", "Terney", "Amursk", "Elban", "Vanino", "Sovetskaya Gavan", "Toki", "De-kastri", "Nikolaevsk", "Pravda", "Kholmsk", "Forel", "Nakano", "Aleksandrovsk", "Yuzhno-Sakhalinsk", "Alba", "Korsakov", "Aniva", "Poronaysk", "Tymovskoye", "Yana", "Magadan", "Muna", "Dukat", "Vilyuchinsk", "Elizovo", "Petropavlovsk-kamchatskiy", "Nagornyy", "Kozyrevsk", "Kluchi", "Ivashka", "Dana", "Leningradskiy", "Anadyr" ]

#dns:
#  <Домен>:
#    default:
#      <subDomain>: <address>
#    <match acl name>:
#      any: <address>
#
# any - Используется как @

dns:
  cdn.domain.loc:
    default:
      any: 5.5.5.5
      moscow: 5.5.5.5
      france: 7.7.7.7
      novosibirsk: 2.2.2.2
      ukraine: 3.3.3.3
    france:
      any: 7.7.7.7
    moscow:
      any: 5.5.5.5
    ukraine:
      any: 3.3.3.3
    novosibirsk:
      any: 2.2.2.2
```

### Генерация с использование docker образа
```shell
docker run --rm -ti \
  -v $(pwd)/config.yml:/config.yml \
  -v $(pwd):/mnt \
  registry.gitlab.com/cw-serv.ru/named-dns-configurator:master generate /config.yml -d /mnt/
```

### Отслеживание достуных серверов и генерация конфига
```shell
docker run --rm -ti \
  -v $(pwd)/config.yml:/config.yml \
  -v $(pwd):/mnt \
  registry.gitlab.com/cw-serv.ru/named-dns-configurator:master watcher /config.yml -d /mnt/
```

### Подключить named-dns-configurator скрипт в Dockerfile
> Требует php >=7.4
```Dockerfile
COPY --from=registry.gitlab.com/cw-serv.ru/named-dns-configurator:master /usr/bin/named-dns-configurator /usr/bin/named-dns-configurator
```